import type { Metadata } from "next";
import localFont from "next/font/local";
import { Header, Footer } from "@/components";

import "./globals.css";

const gilroy = localFont({
  src: [
    {
      path: "_src/fonts/Gilroy-Medium.woff2",
      weight: "500",
      style: "normal",
    },
    {
      path: "_src/fonts/Gilroy-Semibold.woff2",
      weight: "600",
      style: "normal",
    },
    {
      path: "_src/fonts/Gilroy-Bold.woff2",
      weight: "700",
      style: "normal",
    },
    {
      path: "_src/fonts/Gilroy-Black.woff2",
      weight: "900",
      style: "normal",
    },
  ],
  display: "swap",
});

export const metadata: Metadata = {
  title: "Aboba",
  description: "My Perfect Aboba",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={gilroy.className}>
        <Header />
        <main>{children}</main>
        <Footer />
      </body>
    </html>
  );
}
