import { Info, Treds } from "./_sections";

import styles from "./page.module.scss";

export default function Profile() {
  return (
    <div className={styles.wrapper + " container"}>
      <Info />
      <Treds />
    </div>
  );
}
