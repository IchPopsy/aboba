import { User, Connected, Favorite, Later, Rating } from "./_sections/";
import { Treds } from "..";
import Image from "next/image";

import styles from "./Info.module.scss";

const userdata = {
  img: "/img/profile/profileimg.png",
  name: "Cool Man",
  place: "Russia, Lobnya",
  description:
    "Типо описание, очень длинное, очень прикольное и вообще Lorem Ipsum Типо user",
};

export const Info = () => {
  return (
    <div className={styles.left_section}>
      <User {...userdata} />
      <Treds mobile />
      <Connected />
      <Favorite />
      <Later />
      <Rating />
    </div>
  );
};
