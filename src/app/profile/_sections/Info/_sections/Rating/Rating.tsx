import Image from "next/image";

import styles from "./Rating.module.scss";

export const Rating = () => {
  return (
    <section className={styles.rating_section}>
      <div className={styles.title_wrapper}>
        <h2 className={styles.title}>Оценки пользователя</h2>
        <a href="" className={styles.title_link}>
          Полный список
        </a>
      </div>
      <div className={styles.grid_wrapper}>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r10}>10</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r9}>9</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r8}>8</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r7}>7</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r6}>6</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav1.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r5}>5</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav2.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r4}>4</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav1.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r3}>3</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav1.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r2}>2</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav1.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r1}>1</p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <div className={styles.image_mark}>
            <Image
              src="/img/profile/fav1.png"
              width={103}
              height={150}
              className={styles.gridimg}
              alt="gridimg"
            />
            <p className={styles.grid_mark + " " + styles.grid_mark_r0}>
              <svg
                width="16"
                height="7"
                viewBox="0 0 16 7"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M5.23126 3.5C5.23126 2.42611 6.55947 1.55556 8.19791 1.55556C9.83634 1.55556 11.1646 2.42611 11.1646 3.5C11.1646 4.57389 9.83634 5.44444 8.19791 5.44444C6.55947 5.44444 5.23126 4.57389 5.23126 3.5ZM8.19791 2.33333C7.21485 2.33333 6.41792 2.85567 6.41792 3.5C6.41792 4.14433 7.21485 4.66667 8.19791 4.66667C9.18097 4.66667 9.9779 4.14433 9.9779 3.5C9.9779 2.85567 9.18097 2.33333 8.19791 2.33333Z"
                  fill="white"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M2.12492 2.79811C1.79335 3.11126 1.67128 3.35621 1.67128 3.5C1.67128 3.64379 1.79335 3.88874 2.12492 4.20189C2.44543 4.5046 2.92184 4.83274 3.52458 5.13594C4.73269 5.74367 6.38901 6.22222 8.19791 6.22222C10.0068 6.22222 11.6631 5.74367 12.8712 5.13594C13.474 4.83274 13.9504 4.5046 14.2709 4.20189C14.6025 3.88874 14.7245 3.64379 14.7245 3.5C14.7245 3.35621 14.6025 3.11126 14.2709 2.79811C13.9504 2.4954 13.474 2.16726 12.8712 1.86406C11.6631 1.25633 10.0068 0.777778 8.19791 0.777778C6.38901 0.777778 4.73269 1.25633 3.52458 1.86406C2.92184 2.16726 2.44543 2.4954 2.12492 2.79811ZM2.80209 1.24705C4.1709 0.558486 6.07457 0 8.19791 0C10.3213 0 12.2249 0.558486 13.5937 1.24705C14.2794 1.592 14.8457 1.97682 15.2458 2.35467C15.6348 2.72207 15.9112 3.12527 15.9112 3.5C15.9112 3.87473 15.6348 4.27793 15.2458 4.64533C14.8457 5.02318 14.2794 5.408 13.5937 5.75295C12.2249 6.44151 10.3213 7 8.19791 7C6.07457 7 4.1709 6.44151 2.80209 5.75295C2.11637 5.408 1.55009 5.02318 1.15002 4.64533C0.761009 4.27793 0.484619 3.87473 0.484619 3.5C0.484619 3.12527 0.761009 2.72207 1.15002 2.35467C1.55009 1.97682 2.11637 1.592 2.80209 1.24705Z"
                  fill="white"
                />
              </svg>
            </p>
          </div>
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
      </div>
    </section>
  );
};
