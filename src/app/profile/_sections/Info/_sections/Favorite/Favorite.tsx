import Image from "next/image";

import styles from "./Favorite.module.scss";

export const Favorite = () => {
  return (
    <section className={styles.favorite_section}>
      <div className={styles.title_wrapper}>
        <h2 className={styles.title}>Любимые фильмы</h2>
        <a href="" className={styles.title_link}>
          Полный список
        </a>
      </div>
      <div className={styles.grid_wrapper}>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav2.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav2.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav1.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
        <article className={styles.grid_element}>
          <Image
            src="/img/profile/fav2.png"
            width={103}
            height={150}
            className={styles.gridimg}
            alt="gridimg"
          />
          <div className={styles.grid_text}>
            <a href="" className={styles.grid_name}>
              Сверхъественное
            </a>
            <p className={styles.grid_duration}>Supernatural, 43 мин</p>
            <div className={styles.grid_subtext}>
              <p className={styles.grid_country}>
                Страна: США, Режисёр: Роберт Сингер
              </p>
              <p>Жанры: фэнтези, ужасы, триллер</p>
              <p>Годы производства: 2005-2020</p>
            </div>
          </div>
        </article>
      </div>
    </section>
  );
};
