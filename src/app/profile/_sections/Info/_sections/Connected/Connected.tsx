import Image from "next/image";

import styles from "./Connected.module.scss";

export const Connected = () => {
  return (
    <section className={styles.connected_section}>
      <h2 className={styles.connected_title}>Связанные аккаунты</h2>
      <div className={styles.connected_accs}>
        <div className={styles.facebook}>
          <Image
            src="/img/profile/profileimg.png"
            width={100}
            height={100}
            className={styles.user_avatar}
            alt="fbavatar"
          />
          <svg
            className={styles.svg}
            width="30"
            height="30"
            viewBox="0 0 30 30"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M30 15C30 23.2843 23.2843 30 15 30C6.71573 30 0 23.2843 0 15C0 6.71573 6.71573 0 15 0C23.2843 0 30 6.71573 30 15Z"
              fill="url(#paint0_linear_1_377)"
            />
            <path
              d="M20.5861 19.5874L21.2524 15.3537H17.0842V12.6075C17.0842 11.449 17.6654 10.319 19.5323 10.319H21.4286V6.71463C21.4286 6.71463 19.7084 6.42857 18.0646 6.42857C14.6302 6.42857 12.3875 8.45672 12.3875 12.1269V15.3537H8.57143V19.5874H12.3875V29.8227C13.1536 29.9399 13.9374 30 14.7358 30C15.5343 30 16.318 29.9399 17.0842 29.8227V19.5874H20.5861Z"
              fill="white"
            />
            <defs>
              <linearGradient
                id="paint0_linear_1_377"
                x1="15"
                y1="0"
                x2="15"
                y2="29.911"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="#18ACFE" />
                <stop offset="1" stopColor="#0163E0" />
              </linearGradient>
            </defs>
          </svg>
          <div className={styles.text}>
            <h2 className={styles.acc_name}>Cool Man</h2>
            <a href="" className={styles.acc_link}>
              Профиль FaceBook
            </a>
          </div>
        </div>
        <div className={styles.youtube}>
          <Image
            src="/img/profile/profileimg.png"
            width={100}
            height={100}
            className={styles.user_avatar}
            alt="ytavatar"
          />
          <svg
            className={styles.svg}
            width="27"
            height="22"
            viewBox="0 0 27 22"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0.233809 4.22262C0.356716 2.1025 1.87802 0.440964 3.77189 0.335835C6.51434 0.183602 10.4341 0 13.3875 0C16.3409 0 20.2607 0.183602 23.0031 0.335835C24.897 0.440964 26.4183 2.1025 26.5412 4.22262C26.6589 6.25385 26.775 8.75321 26.775 10.7143C26.775 12.6754 26.6589 15.1747 26.5412 17.206C26.4183 19.3261 24.897 20.9876 23.0031 21.0927C20.2607 21.245 16.3409 21.4286 13.3875 21.4286C10.4341 21.4286 6.51434 21.245 3.77189 21.0927C1.87803 20.9876 0.356716 19.3261 0.233809 17.206C0.116054 15.1747 0 12.6754 0 10.7143C0 8.75321 0.116054 6.25385 0.233809 4.22262Z"
              fill="#FF0000"
            />
            <path
              d="M10.5187 6.42857V15L18.1687 10.7143L10.5187 6.42857Z"
              fill="white"
            />
          </svg>
          <div className={styles.text}>
            <h2 className={styles.acc_name}>Cool Man</h2>
            <a href="" className={styles.acc_link}>
              Профиль Youtube
            </a>
          </div>
        </div>
        <div className={styles.vkontakte}>
          <Image
            src="/img/profile/profileimg.png"
            width={100}
            height={100}
            className={styles.user_avatar}
            alt="vkavatar"
          />
          <svg
            className={styles.svg}
            width="30"
            height="30"
            viewBox="0 0 30 30"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0 10.2857C0 6.68538 0 4.88521 0.700672 3.51006C1.317 2.30045 2.30045 1.317 3.51006 0.700672C4.88521 0 6.68538 0 10.2857 0H19.7143C23.3146 0 25.1148 0 26.4899 0.700672C27.6996 1.317 28.683 2.30045 29.2993 3.51006C30 4.88521 30 6.68538 30 10.2857V19.7143C30 23.3146 30 25.1148 29.2993 26.4899C28.683 27.6996 27.6996 28.683 26.4899 29.2993C25.1148 30 23.3146 30 19.7143 30H10.2857C6.68538 30 4.88521 30 3.51006 29.2993C2.30045 28.683 1.317 27.6996 0.700672 26.4899C0 25.1148 0 23.3146 0 19.7143V10.2857Z"
              fill="#2789F6"
            />
            <path
              d="M22.4178 17.5008C22.187 17.1876 22.1353 16.8706 22.2615 16.5494C22.5762 15.6924 27.16 10.2494 25.2505 10.2494C23.9383 10.2669 22.3603 10.2494 21.1023 10.2494C20.8687 10.3063 20.7176 10.4288 20.6231 10.6782C19.8874 12.3744 18.9872 14.4444 17.6679 15.7748C17.4944 15.9147 17.3861 15.9088 17.1884 15.857C16.263 14.8827 17.2498 11.5447 16.687 10.2258C16.576 9.96759 16.3333 9.85098 16.0849 9.78532C14.8285 9.48588 11.9589 9.66412 11.4908 10.2669C11.3569 10.4392 11.3382 10.5332 11.4349 10.5487C11.881 10.619 12.1968 10.7874 12.3826 11.0536C12.7333 11.8257 12.9917 15.939 11.9589 15.939C10.9262 15.939 9.26092 11.9023 8.80344 10.7832C8.68169 10.4463 8.37177 10.2531 8.04522 10.1961L4.98996 10.2195C4.45292 10.2195 4.15278 10.4844 4.34312 11.0181C5.93443 14.9682 9.41676 22.6247 14.7578 22.433C15.2928 22.433 16.2133 22.6384 16.6311 22.1746C17.2054 21.4127 16.505 20.0559 17.4897 19.5385C17.7381 19.4063 17.9955 19.5171 18.2088 19.6795C19.3041 20.5135 19.8856 21.9774 21.2363 22.4218C21.4738 22.5 21.6819 22.5196 21.8606 22.4804L24.715 22.4336C25.245 22.4336 25.767 22.1978 25.6574 21.5642C25.2412 20.2055 23.583 18.8089 22.4178 17.5008Z"
              fill="white"
            />
          </svg>
          <div className={styles.text}>
            <h2 className={styles.acc_name}>Cool Man</h2>
            <a href="" className={styles.acc_link}>
              Профиль VK
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};
