import React from "react";
import Image from "next/image";
import Link from "next/link";

import styles from "./User.module.scss";

interface IUser {
  img: string;
  name: string;
  place: string;
  description: string;
}

export const User: React.FC<IUser> = ({ img, name, place, description }) => {
  return (
    <section className={styles.user_section}>
      <h2 className={styles.left_title}>Информация об пользователе</h2>
      <div className={styles.user_wrapper}>
        <Image
          src={img}
          width={150}
          height={150}
          className={styles.user_avatar}
          alt="avatar"
        />
        <div className={styles.text}>
          <h2 className={styles.user_name}>{name}</h2>
          <p className={styles.user_location}>
            <svg
              width="12"
              height="12"
              viewBox="0 0 12 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M5.98296 0C2.95873 0 0.498291 1.87119 0.498291 4.17114C0.498291 7.06194 5.98835 12 5.98835 12C5.98835 12 11.4676 6.91978 11.4676 4.17114C11.4676 1.87119 9.00727 0 5.98296 0ZM7.63779 5.39246C7.18149 5.7394 6.58227 5.91292 5.98296 5.91292C5.38374 5.91292 4.78432 5.7394 4.32821 5.39246C3.41571 4.69856 3.41571 3.56946 4.32821 2.87549C4.77007 2.53931 5.35783 2.35415 5.98296 2.35415C6.60808 2.35415 7.19575 2.53938 7.63779 2.87549C8.5503 3.56946 8.5503 4.69856 7.63779 5.39246Z"
                fill="white"
              />
            </svg>
            <Link
              target="_blank"
              href={`https://yandex.ru/maps/?text=${place}`}
            >
              {" "}
              {place}
            </Link>
          </p>
          <p className={styles.user_description}>{description}</p>
        </div>
      </div>
    </section>
  );
};

const place = () => {};
