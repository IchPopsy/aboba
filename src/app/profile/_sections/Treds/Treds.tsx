import React from "react";
import Image from "next/image";

import styles from "./Treds.module.scss";

interface ITreds {
  mobile?: boolean;
}

export const Treds: React.FC<ITreds> = ({ mobile }) => {
  return (
    <div
      className={
        styles.right_section + " " + (mobile ? styles.right_section_mobile : "")
      }
    >
      <h2 className={styles.right_title}>Треды пользователя</h2>
      <section className={styles.scroll_section}>
        <div className={styles.grid}>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
          <article className={styles.grid_content}>
            <Image
              src="/img/forum/forum.png"
              width={213}
              height={180}
              className={styles.forum_img}
              alt="newsbg"
            />
            <div className={styles.grid_rigth}>
              <p className={styles.grid_title}>КиноВселенная “Майора Грома”</p>
              <p className={styles.grid_text}>
                В данном треде, Вы сможете узнать намного больше про
                киновселенную от Bubble про великого майора полиции - Игоря
                Грома
              </p>
              <a href="" className={styles.grid_btn}>
                Подробнее →
              </a>
            </div>
          </article>
        </div>
      </section>
    </div>
  );
};
