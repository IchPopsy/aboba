import Image from "next/image";

import styles from "./News.module.scss";

export const News = () => {
  return (
    <section className={styles.news_section}>
      <Image
        src="/img/news/news_bg.svg"
        width={1904}
        height={1201}
        className={styles.news_bg}
        alt="newsbg"
      />
      <div className={styles.news + " container"}>
        <div className={styles.news_left}>
          <Image
            className={styles.news_pic}
            src="/img/news/supernatyralnews.png"
            width={704}
            height={470}
            alt="newspic"
          />
          <p className={styles.news_pic_title}>
            The CW заказал пилотные серии «Рыцарей Готэма» и спин-оффа о
            родителях Винчестеров
          </p>
        </div>
        <div className={styles.news_text}>
          <div className={styles.news_text_top}>
            <h2>Новости</h2>
            <a href="" className={styles.news_text_top_href}>
              Читать все
            </a>
          </div>
          <div className={styles.news_text_grid}>
            <div className={styles.news_text_grid_content}>
              <Image
                className={styles.news_text_grid_content_img}
                src="/img/news/legonews.png"
                width={148}
                height={148}
                alt="news1"
              />
              <div className={styles.news_text_grid_content_fill}>
                <h3>Дисней выпустит первый хоррор</h3>
                <p>
                  Равным образом рамки и место обучения кадров обеспечивает
                  широкому кругу (специалистов) участие в формировании
                  дальнейших направлений развития...
                </p>
              </div>
            </div>
            <div className={styles.news_text_grid_content}>
              <Image
                className={styles.news_text_grid_content_img}
                src="/img/news/legonews.png"
                width={148}
                height={148}
                alt="news1"
              />
              <div className={styles.news_text_grid_content_fill}>
                <h3>Дисней выпустит первый хоррор</h3>
                <p>
                  Равным образом рамки и место обучения кадров обеспечивает
                  широкому кругу (специалистов) участие в формировании
                  дальнейших направлений развития...
                </p>
              </div>
            </div>
            <div className={styles.news_text_grid_content}>
              <Image
                className={styles.news_text_grid_content_img}
                src="/img/news/legonews.png"
                width={148}
                height={148}
                alt="news1"
              />
              <div className={styles.news_text_grid_content_fill}>
                <h3>Дисней выпустит первый хоррор</h3>
                <p>
                  Равным образом рамки и место обучения кадров обеспечивает
                  широкому кругу (специалистов) участие в формировании
                  дальнейших направлений развития...
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
