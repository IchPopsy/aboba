import Image from "next/image";
import { Button } from "@/components";

import styles from "./WatchLater.module.scss";

export const WatchLater = () => {
  return (
    <section className={styles.watchlater_sections}>
      <div className={styles.watchlater + " container"}>
        <h2 className={styles.watchlater_title}>Вы хотели посмотреть</h2>
        <div className={styles.films}>
          <div className={styles.card}>
            <Image
              className={styles.img}
              src="/img/watchlater/later.png"
              width={349.5}
              height={495}
              alt="supernatural"
            />
            <Button className={styles.btn}>Посмотреть</Button>
          </div>
          <div className={styles.card}>
            <Image
              className={styles.img}
              src="/img/watchlater/later.png"
              width={349.5}
              height={495}
              alt="supernatural"
            />
            <Button className={styles.btn}>Посмотреть</Button>
          </div>
          <div className={styles.card}>
            <Image
              className={styles.img}
              src="/img/watchlater/later.png"
              width={349.5}
              height={495}
              alt="supernatural"
            />
            <Button className={styles.btn}>Посмотреть</Button>
          </div>
        </div>
        <a href="" className={styles.full_list}>
          Полный список
        </a>
      </div>
    </section>
  );
};
