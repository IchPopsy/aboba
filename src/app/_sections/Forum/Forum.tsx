import Image from "next/image";

import styles from "./Forum.module.scss";
import { styleText } from "util";
import { Button } from "@/components";

export const Forum = () => {
  return (
    <section className={styles.forum_section}>
      <Image
        className={styles.forum_bg}
        src="/img/forum/forum_bg.svg"
        width={1904}
        height={200}
        alt="fbg"
      />

      <div className={styles.forum_top + " container"}>
        <div className={styles.forum_title}>
          <h2>Форум</h2>
          <a href="" className={styles.forum_href}>
            Все треды
          </a>
        </div>
      </div>
      <div className={styles.forum_bot}>
        <div className=" container">
          <div className={styles.grid_top}>
            <article className={styles.grid_content}>
              <Image
                src="/img/forum/forum.png"
                width={365.47}
                height={308}
                className={styles.forum_img}
                alt="newsbg"
              />
              <div className={styles.grid_rigth}>
                <p className={styles.grid_title}>
                  КиноВселенная “Майора Грома”
                </p>
                <p className={styles.grid_text}>
                  В данном треде, Вы сможете узнать намного больше про
                  киновселенную от Bubble про великого майора полиции - Игоря
                  Грома
                </p>
                <a href="" className={styles.grid_btn}>
                  Подробнее →
                </a>
              </div>
            </article>
            <article className={styles.grid_content}>
              <div className={styles.grid_rigth}>
                <p className={styles.grid_title}>
                  Каким будет новый фильм от марвел?
                </p>
                <p className={styles.grid_text}>
                  Скоро на экраны выходит новый фильм от Sony Marvel, хотелось
                  бы порассуждать, а каким он будет? Если у вас есть какие-либо
                  заметки, милости прошу к нашему шалашу...
                </p>
                <a href="" className={styles.grid_btn}>
                  Подробнее →
                </a>
              </div>
            </article>
          </div>
          <div className={styles.grid_bot}>
            <article className={styles.grid_content}>
              <div className={styles.grid_rigth}>
                <p className={styles.grid_title}>
                  Каким будет новый фильм от марвел?
                </p>
                <p className={styles.grid_text}>
                  Скоро на экраны выходит новый фильм от Sony Marvel, хотелось
                  бы порассуждать, а каким он будет? Если у вас есть какие-либо
                  заметки, милости прошу к нашему шалашу...
                </p>
                <a href="" className={styles.grid_btn}>
                  Подробнее →
                </a>
              </div>
            </article>
            <article className={styles.grid_content}>
              <Image
                src="/img/forum/forum.png"
                width={365.47}
                height={308}
                className={styles.forum_img}
                alt="newsbg"
              />
              <div className={styles.grid_rigth}>
                <p className={styles.grid_title}>
                  КиноВселенная “Майора Грома”
                </p>
                <p className={styles.grid_text}>
                  В данном треде, Вы сможете узнать намного больше про
                  киновселенную от Bubble про великого майора полиции - Игоря
                  Грома
                </p>
                <a href="" className={styles.grid_btn}>
                  Подробнее →
                </a>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
  );
};
