import Image from "next/image";
import { Button } from "@/components";

import styles from "./Hero.module.scss";

export const Hero = () => {
  return (
    <section className={styles.hero_section}>
      <Image
        className={styles.hero_bg}
        src="/img/hero/hero_bg.svg"
        width={711}
        height={1466}
        alt="hbg"
      />
      <div className={styles.hero + " container"}>
        <h1 className={styles.hero_title}>
          Фильмы, сериалы, мультфильмы в&nbsp;одном&nbsp;месте!
        </h1>
        <p className={styles.hero_text}>
          Всегда хотели узнать где можно дешевле посмотреть то или иное
          произведение исскуства? Просто обсудить какой-то либо кинофраншизу? А
          может просто посмотреть последние новости в сфере кино? <br />
          Тогда, данный сайт был сделан для вас!
        </p>
        <Button withShadow className={styles.hero_btn}>
          Посмотреть каталог фильмов
        </Button>
      </div>
    </section>
  );
};
