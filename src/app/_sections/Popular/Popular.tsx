"use client";

import React, { useState, useEffect } from "react";
import Image from "next/image";
import { Button } from "@/components";

import styles from "./Popular.module.scss";

const slides = [
  {
    src: "/img/popular/slide_1.png",
    alt: "slide1",
  },
  {
    src: "/img/popular/slide_2.png",
    alt: "slide2",
  },
  {
    src: "/img/popular/slide_3.png",
    alt: "slide3",
  },
  {
    src: "/img/popular/slide_4.png",
    alt: "slide4",
  },
];

export const Popular = () => {
  const [activeSlide, setActiveSlide] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveSlide((prevSlide) => (prevSlide + 1) % slides.length);
    }, 4000);

    return () => clearInterval(interval);
  }, []);

  return (
    <section className={styles.popular_section}>
      <div className="container">
        <h2 className={styles.popular_title}>
          Самый популярный фильм на неделе
        </h2>
        <div className={styles.slider}>
          {slides.map((slide, index) => (
            <Image
              className={`${styles.img} ${
                index === activeSlide ? styles.active : ""
              }`}
              src={slide.src}
              width={976}
              height={460}
              alt={slide.alt}
              key={index}
            />
          ))}
          <Button className={styles.btn}>Посмотреть</Button>
        </div>
      </div>
      <Image
        className={styles.popular_bg}
        src="/img/popular/popular_bg.svg"
        width={454}
        height={1380}
        alt="slide1"
      />
    </section>
  );
};
