import { Hero, Popular, WatchLater, News, Forum } from "./_sections";

export default function Home() {
  return (
    <>
      <Hero />
      <Popular />
      <WatchLater />
      <News />
      <Forum />
    </>
  );
}
