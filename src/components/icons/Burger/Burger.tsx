import styles from "./Burger.module.scss";

interface IBurger {
  isActive: boolean;
  onClick: () => void;
}

export const Burger: React.FC<IBurger> = ({ isActive, onClick }) => {
  return (
    <button
      className={
        styles.burger + " mobile" + " " + (isActive ? styles.active : "")
      }
      onClick={onClick}
    >
      <div className={styles.bar}></div>
      <div className={styles.bar}></div>
      <div className={styles.bar}></div>
    </button>
  );
};
