import styles from "./Input.module.scss";

export const Input = () => {
  return (
    <div className={styles.wrapper}>
      <input type="email" placeholder="Ваш email" />
      <button className={styles.btn}>Подписаться</button>
    </div>
  );
};
