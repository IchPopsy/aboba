import React from "react";

import styles from "./Button.module.scss";

interface IButton {
  withShadow?: boolean;
  className?: string;
  children: string;
}

export const Button: React.FC<IButton> = ({
  withShadow,
  className,
  children,
}) => {
  return (
    <button
      className={
        styles.button +
        " " +
        (withShadow ? styles.button_withshadow : "") +
        " " +
        (className ? className : "")
      }
    >
      {children}
    </button>
  );
};
