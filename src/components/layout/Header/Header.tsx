"use client";

import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

import { Search, Login, Burger } from "@/components";

import styles from "./Header.module.scss";

export const Header = () => {
  const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false);

  return (
    <header>
      <div className={styles.header + " container"}>
        <Link href="/">
          <Image
            className={styles.header_logo}
            src="/img/logo.png"
            width={250}
            height={120}
            alt="logo"
          />
        </Link>

        <nav className={styles.header_nav}>
          <ul
            className={
              styles.header_list +
              " " +
              (isOpenMenu ? styles.header_list_active : "")
            }
          >
            <li>
              <a href="">Фильмы</a>
            </li>
            <li>
              <a href="">Треды</a>
            </li>
            <li>
              <a href="">Новости</a>
            </li>
          </ul>
        </nav>

        <div className={styles.header_btns}>
          <Search />
          <Login />
          <Burger
            isActive={isOpenMenu}
            onClick={() => setIsOpenMenu(!isOpenMenu)}
          />
        </div>
      </div>
    </header>
  );
};
