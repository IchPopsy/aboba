import Image from "next/image";
import { Input } from "@/components";

import styles from "./Footer.module.scss";

export const Footer = () => {
  return (
    <footer className={styles.footer_bg}>
      <div className={styles.footer_top + " container"}>
        <p className={styles.text}>
          Подпишитесь на нашу рассылку прямо сейчас, всего за{" "}
          <span>9.99$/мес</span>, и будьте в курсе всех новостей, скидок на
          подписки и понижении цен на фильмы.
        </p>
        <Input />
        <p className={styles.privacy}>
          Нажимая на кнопку “Подписаться”, вы принимаете правила{" "}
          <a href="">
            <span>пользовательского соглашения</span>
          </a>
        </p>
      </div>
      <div className={styles.footer + " container"}>
        <Image
          className={styles.footer_logo}
          src="/img/logo.png"
          width={250}
          height={120}
          alt="logo"
        />

        <nav className={styles.footer_nav}>
          <ul className={styles.footer_list}>
            <li>
              <a href="">Фильмы</a>
            </li>
            <li>
              <a href="">Треды</a>
            </li>
            <li>
              <a href="">Новости</a>
            </li>
          </ul>
        </nav>
      </div>
    </footer>
  );
};
